var gulp = require('gulp'),
   util = require('gulp-util'),
   sass = require('gulp-sass'),
   browserSync = require('browser-sync'),
   autoprefixer = require('gulp-autoprefixer'),
   sourcemaps = require('gulp-sourcemaps'),
   uglify = require('gulp-uglify'),
   jshint = require('gulp-jshint'),
   header  = require('gulp-header'),
   rename = require('gulp-rename'),
   cssnano = require('gulp-cssnano'),
   package = require('./package.json');

//default config
var config = {
   assetsDir: 'app/Resources/assets',
   sassPattern: 'sass/**/*.scss',
   production: !!util.env.production
   //Those two exclamations turn undefined into a proper false.
};


var supported = [
   'last 4 versions',
   'safari >= 8',
   'ie >= 10',
   'ff >= 20',
   'ios 6',
   'android 4'
];



var banner = [
 '/*!\n' +
 ' * <%= package.name %>\n' +
 ' * <%= package.title %>\n' +
 ' * <%= package.url %>\n' +
 ' * @author <%= package.author %>\n' +
 ' * @version <%= package.version %>\n' +
 ' * Copyright ' + new Date().getFullYear() + '. <%= package.license %> licensed.\n' +
 ' */',
 '\n'
].join('');

gulp.task('css', function () {
   return gulp.src('src/assets/scss/style.scss')
   .pipe(!!util.env.production ? util.noop() : sourcemaps.init() )
   .pipe(sass({errLogToConsole: true}))
   .pipe(autoprefixer('last 4 version'))
   .pipe(gulp.dest('assets/css'))
   .pipe(cssnano())
   /*.pipe(cssnano({
           autoprefixer: {browsers: supported, add: true}
       }))*/
   .pipe(rename({ suffix: '.min' }))
   //no sourcemap for production
   .pipe(!!util.env.production ? util.noop() : sourcemaps.write() )
   .pipe(gulp.dest('assets/css'))
   .pipe(browserSync.reload({stream:true}));
});



gulp.task('js',function(){
 gulp.src('src/assets/js/scripts.js')
   .pipe(jshint('.jshintrc'))
   .pipe(jshint.reporter('default'))
   .pipe(header(banner, { package : package }))
   .pipe(gulp.dest('assets/js'))
   .pipe(uglify())
   .pipe(uglify().on('error', function(e) { console.log('\x07',e.message); return this.end(); }))
   .pipe(rename({ suffix: '.min' }))
   .pipe(gulp.dest('assets/js'))
   .pipe(browserSync.reload({stream:true, once: true}));
});

gulp.task('browser-sync', function() {
   browserSync.init(null, {
       proxy : "http://localhost/eup/"
   });
});
gulp.task('bs-reload', function () {
   browserSync.reload();
});

gulp.task('default', ['css', 'js', 'browser-sync'], function () {
   gulp.watch("src/assets/scss/*/*.scss", ['css', 'bs-reload']);
   gulp.watch("src/assets/js/*.js", ['js', 'bs-reload']);
});